import cv2 as cv

# Leer imagen circulo
img1=cv.imread('./circulo.jpeg')
# Leer imagen rectangulo
img2=cv.imread('./rectangulo.jpeg')
#Redimensiona las imagenes
imagen1_res = cv.resize(img1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
imagen2_res = cv.resize(img2, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
print(imagen1_res.shape)
print(imagen2_res.shape)
# Realizar operaciones solicitadas
res = cv.bitwise_and(imagen1_res,imagen2_res)
#res = cv.bitwise_or(imagen1_res,imagen2_res)
#res = cv. bitwise_not(imagen1_res)
#res = cv.bitwise_xor(imagen1_res,imagen2_res)
#cv.imshow('imag1', img1)
#cv.imshow('imag2', img2)
cv.imshow('resultado', res)
cv.waitKey(0)#espera presionar una tecla para que la ventana desaparezca