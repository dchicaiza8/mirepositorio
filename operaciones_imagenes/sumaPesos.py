import cv2 as cv
# Usar OpenCV para Adición de imágenes
# cv.add
# Usar OpenCV para Mezcla de imágenes
# cv.addWeughted
# Usar OpenCV para Sustracción  de imágenes
# cv.substract
# cv.absdiff

# Leer imagen Simón Bolívar
img1=cv.imread('./mitad_mundo.jpg')
# Leer imagen Torre Eiffel
img2=cv.imread('./torre_eiffel.jpg')
# Realizar operaciones solicitadas
print(img1.shape)
print(img2.shape)
imagen1_res = cv.resize(img1, dsize=(512, 512), interpolation=cv.INTER_CUBIC)
suma = cv.addWeighted(imagen1_res,0.2,img2,0.8,0.5)
cv.imshow('imag1', img1)#muestra la imagen a blanco y negro en pantalla con el titulo binaria
cv.imshow('imag2', img2)#muestra la imagen a blanco y negro en pantalla con el titulo binaria
cv.imshow('suma', suma)#muestra la imagen a blanco y negro en pantalla con el titulo binaria
cv.waitKey(0)#espera presionar una tecla para que la ventana desaparezca
cv.destroyAllWindows()#Cierra la ventana

