import cv2 as cv
#probando commit
#imagen = cv.imread('./imagenes/lena.png')#leer imagen con libreria opencv
#cv.imshow('Leer y mostrar imagen con opencv',imagen)#muestra imagen en pantalla con el titulo que esta entre comillas
#dos puntos accede a un directorio externo al proyecto
im_gray = cv.imread('../externo/circulo.jpg', cv.IMREAD_GRAYSCALE)#lectura de imagen y transformacion a escala de grises
#la siguiente linea convierte la imagen en escala de grises a binaria que determina el umbral automaticamente a partir de la imagen utilizando el metodo Otsu
(thresh, im_bw) = cv.threshold(im_gray, 128, 255, cv.THRESH_BINARY | cv.THRESH_OTSU)
#Si ya conoce el umbral, puede utilizar las dos siguientes lineas
#thresh = 127
#im_bw = cv.threshold(im_gray, thresh, 255, cv.THRESH_BINARY)[1]
cv.imshow('Gris', im_gray)#muestra la imagen en escala de grises en pantalla con el titulo Gris
cv.imwrite('./Salida/grises1.jpg', im_gray)#guarda imagen en escala de grises en el directorio Salida
cv.imshow('binaria', im_bw)#muestra la imagen a blanco y negro en pantalla con el titulo binaria
cv.imwrite('./Salida/binary1.jpg', im_bw)#guarda imagen binaria en el directorio Salida
cv.waitKey(0)#espera presionar una tecla para que la ventana desaparezca
cv.destroyAllWindows()#Cierra la ventana